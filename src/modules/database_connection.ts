
import { useContainer as ormUseContainer } from "typeorm/container";
import { Container as ExtensionContainer } from "typeorm-typedi-extensions";
import { ConnectionOptions, createConnection } from "typeorm";
import { Product } from "../entities/Product";
import { MicroframeworkSettings } from "microframework-w3tec";

/**
 * Database connection handler
 */
export async function database_connection(settings: MicroframeworkSettings) {
    ormUseContainer(ExtensionContainer);
        const connection = await createConnection(<ConnectionOptions>{
            "cli": {
                "entitiesDir": "src/entity",
                "migrationsDir": "src/migration",
                "subscribersDir": "src/subscriber"
            },
            "database": process.env.MONGODB_NAME,
            "entities": [
                `${__dirname}/../../dist/entities/*.js`
            ],
            "host": process.env.MONGODB_HOST,
            "logging": true,
            "migrations": [
                "src/migration/*.js"
            ],
            "port": Number(process.env.MONGODB_PORT),
            "subscribers": [
                "src/subscriber/*.js"
            ],
            "synchronize": true,
            "type": "mongodb",
            "useUnifiedTopology": true
        });
        settings.onShutdown(() => {
            console.log("Stop Connection");
            connection.close();
        });
        //
        //     const newP = new Product();
        //     newP.name = "Foo";
        //     await newP.save();
        //     console.log(newP);
        //
            const prod = await connection.mongoManager.find(Product);
            console.log(prod, prod[0].name);
}