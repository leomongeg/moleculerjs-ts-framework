/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/18/19
 */
import { ServiceEvent }     from "../core/decorators/ServiceEvent";
import { BaseServiceEvent } from "../core/BaseServiceEvent";

/**
 * @TODO: This is a demonstration ServiceEvent Class please DELETE.
 */
export class AcmeEvents extends BaseServiceEvent {

    /**
     * Test event
     * @param payload
     */
    @ServiceEvent()
    private async onAddResultEvent(payload: any) {
        console.log("Event triggered");
        console.log(payload);
    }
}