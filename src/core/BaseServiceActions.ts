/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/16/19
 */

import { ServiceBroker }               from "moleculer";
import { getMongoManager, ObjectType } from "typeorm";

export abstract class BaseServiceActions {
    protected broker: ServiceBroker;

    constructor(broker: ServiceBroker) {
        this.broker = broker;
    }

    /**
     * Get Custom repository from the MongoManagerInstance.
     * Example: const userRepo = this.getRepository(UserRepository);
     *
     * @param customRepository
     */
    protected getRepository<T>(customRepository: ObjectType<T>): T {
        return getMongoManager().getCustomRepository(customRepository);
    }

    /**
     * Build the success response object.
     *
     * @param data
     */
    protected success(data: any) {
        return {
            data  : data,
            error : {},
            status: true
        };
    }

    /**
     * Build the error response object.
     *
     * @param error
     */
    protected error(error: any) {
        return {
            data  : {},
            error : error,
            status: false
        };
    }
}