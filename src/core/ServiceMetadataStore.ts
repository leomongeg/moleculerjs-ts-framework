/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/16/19
 */

import {
    ActionSchema as Action,
    ServiceAction,
    ServiceActionsSchema as Actions,
    ServiceEvent,
    ServiceEvents
} from "moleculer";
import { BaseServiceActions } from "./BaseServiceActions";

/**
 * Actions metadata format Interface.
 */
export interface ActionMetadata {
    name: string;
    actionDefinition: Action;
}

/**
 * Events metadata format Interface.
 */
export interface EventMetadata {
    name: string;
    eventDefinition: ServiceEvent;
}

export class ServiceMetadataStore {
    private static _instance: ServiceMetadataStore;
    private readonly actions: ActionMetadata[];
    private readonly events: EventMetadata[];
    private initializedActions: boolean;
    private initializedEvents: boolean;
    private readonly controllers: BaseServiceActions[];

    private constructor() {
        this.initializedActions = false;
        this.initializedEvents  = false;
        this.actions            = [];
        this.events             = [];
        this.controllers        = [];
    }

    /**
     * Return a instance of the Actions Metadata Store
     *
     * @constructor
     * @return {ServiceMetadataStore}
     */
    public static get Instance(): ServiceMetadataStore {
        return this._instance || (this._instance = new this());
    }

    /**
     * Add new microservice Action to the store.
     *
     * @param action
     * @return {ServiceMetadataStore}
     */
    public addAction(action: ActionMetadata): ServiceMetadataStore {
        if (this.initializedActions) throw "Once that the service is initialized you are unable to register new actions in the core.service";
        this.actions.push(action);
        return this;
    }

    /**
     * Publish the Actions metadata in the core service.
     *
     * @param actions
     */
    public registerActionsInCoreService(actions: Actions): void {
        if (this.initializedActions) throw "The BaseCoreService Only can be implemented once, this behavior (registerActionsInCoreService) should be handled by the core.service handler";

        let actionsCount = this.actions.length;

        while (actionsCount > 0) {
            const action         = this.actions.pop();
            if (!action || actions[action.name]) continue;
            actions[action.name] = action.actionDefinition;
            actionsCount--;
        }

        this.initializedActions = true;
    }

    /**
     * Add new microservice Event to the store.
     *
     * @param event
     * @return {ServiceMetadataStore}
     */
    public addEvent(event: EventMetadata): ServiceMetadataStore {
        if (this.initializedActions) throw "Once that the service is initialized you are unable to register new events in the core.service";
        this.events.push(event);
        return this;
    }

    /**
     * Publish the Events metadata in the core service.
     *
     * @param events
     */
    public registerEventsInCoreService(events: ServiceEvents): void {
        if (this.initializedEvents) throw "The BaseCoreService Only can be implemented once, this behavior (registerEventsInCoreService) should be handled by the core.service handler";

        let eventsCount = this.events.length;

        while (eventsCount > 0) {
            const event                = this.events.pop();
            if (!event) continue;
            const name                 = `${event.name}`;
            event.eventDefinition.name = name;
            events[name]               = event.eventDefinition;
            eventsCount--;
        }

        this.initializedEvents = true;
    }

    /**
     * Re
     */
    public registerServiceController(controller: BaseServiceActions): void {
        this.controllers.push(controller);
    }

}