/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/21/19
 */

export type GatewayRouting = {
    publisher: string,
    publisherAvailableEvt: string,
    routes: GatewayRoute[],
    unpublisher: string
};

export type GatewayRoute = {
    aliases: object,
    authorization?: boolean,
    bodyParsers?: object,
    callOptions?: object,
    cors?: any,
    id: string,
    mappingPolicy?: string,
    path?: string,
    use?: { className: string, params?: any }[],
    publisher?: string,
    publisherAvailableEvt?: string,
    unpublisher?: string
};