/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/16/19
 */

import { BaseService }                                                 from "./BaseService";
import { ServiceBroker, ServiceEvents, ServiceMethods, ServiceSchema } from "moleculer";
import { ServiceMetadataStore }                                        from "./ServiceMetadataStore";
import { getMongoManager, ObjectType }                                 from "typeorm";
import { GatewayRoutingHandler }                                       from "./GatewayRoutingHandler";
import { Container }                                                   from "typedi";
import { importClassesFromDirectories, PlatformTools } from "../util/Importer";

export abstract class BaseCoreService extends BaseService {
    /**
     * Override this property name to define your service name.
     */
    protected static readonly SERVICE_NAME: string  = "<your service name here, plase override in core.service.ts>";
    public static readonly SERVICE_NAME_KEY: string = "SERVICE_NAME";

    /**
     * See ServiceSchema for more information:
     */
    public readonly events: ServiceEvents;
    protected created?: () => void;
    protected serviceStarted: ((broker: ServiceBroker) => void) | undefined;
    private readonly started?: () => any;
    private readonly stopped?: () => any;
    protected mixins?: Array<ServiceSchema>;
    protected methods?: ServiceMethods;
    private controllers: any[];
    private serviceEvents: any[];

    constructor(broker: ServiceBroker) {
        super(broker);

        this.actions       = {};
        this.events        = {};
        this.controllers   = [];
        this.serviceEvents = [];
        this.schema        = <ServiceSchema>{};

        this.registerGatewayEvents();
        this.registerControllers();
        this.registerEvents();
        this.init();
        this.registerGatewayRequestEvents();

        Container.set(BaseCoreService.SERVICE_NAME_KEY, this.name);

        this.stopped = async () => {
            GatewayRoutingHandler.Instance.unpublishRoutes(this.broker);
            return this.serviceStopped();
        };

        this.started = async () => {
            setTimeout(() => {
                            this.broker.logger.info("Sending Routes to API Gateway");
                            GatewayRoutingHandler.Instance.publishRoutes(broker);
                        }, 1000);

            if (this.serviceStarted !== undefined) this.serviceStarted(this.broker);
        };

        ServiceMetadataStore.Instance.registerActionsInCoreService(this.actions);
        ServiceMetadataStore.Instance.registerEventsInCoreService(this.events);

        this.schema = {
            ...{
                actions     : this.actions,
                created     : this.created,
                dependencies: this.dependencies,
                events      : this.events,
                methods     : this.methods,
                mixins      : this.mixins,
                name        : this.name,
                settings    : this.settings,
                started     : this.started,
                stopped     : this.stopped,
                version     : this.version
            }, ...this.schema
        };

        this.parseServiceSchema(this.schema);
    }

    /**
     * Register new events into the base core service.
     */
    private registerEvents(): void {
        const events = importClassesFromDirectories(["dist/events/*{.js,.ts}"]);
        for (const eventHandler of events) {
            this.serviceEvents.push(new eventHandler(this.broker));
        }
    }

    /**
     * Register the api gateway events to core service.
     */
    private registerGatewayEvents() {
        const events = GatewayRoutingHandler.Instance.getGatewayPublisherEvents();

        for (const event of events) {
            this.events[event] = () => {
                GatewayRoutingHandler.Instance.publishRoutes(this.broker);
            };
        }
    }

    /**
     * Register the fixer event to request routes by the Gateway in case that the transport fails.
     * And the node needs to update the state.
     */
    private registerGatewayRequestEvents() {
        const events = GatewayRoutingHandler.Instance.getGatewayPublisherEvents();
        // This is a fix in case that the node is disconnected or the transport fail, recover the routing.
        for (const event of events) {
            const fixEvent = event.replace("publisherAvailable", `${this.name}.requestRoutes`);
            this.events[fixEvent] = () => {
                this.broker.logger.info(`Updating the Gateway Routes for ${fixEvent} Event`);
                GatewayRoutingHandler.Instance.publishRoutes(this.broker);
            };
        }
    }

    /**
     * Load all the service configurations
     */
    protected abstract init(): void;

    /**
     * Register new controller into the base core service.
     */
    private registerControllers(): void {
        const controllers = importClassesFromDirectories(["dist/actions/*{.js,.ts}"]);
        for (const controller of controllers) {
            this.controllers.push(new controller(this.broker));
        }
    }

    /**
     * Get Custom repository from the MongoManagerInstance.
     * Example: const userRepo = this.getRepository(UserRepository);
     *
     * @param customRepository
     */
    protected getRepository<T>(customRepository: ObjectType<T>): T {
        return getMongoManager().getCustomRepository(customRepository);
    }

    /**
     * Build the success response object.
     *
     * @param data
     */
    protected success(data: any) {
        return {
            data  : data,
            error : {},
            status: true
        };
    }

    /**
     * Build the error response object.
     *
     * @param error
     */
    protected error(error: any) {
        return {
            data  : {},
            error : error,
            status: false
        };
    }

    /**
     * Stopped event
     */
    protected async serviceStopped() {
    }
}