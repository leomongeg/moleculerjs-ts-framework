import { Container } from "typedi";
import { importClassesFromDirectories } from "../util/Importer";
import { bootstrapMicroframework, Microframework } from "microframework-w3tec";

export class MicroserviceModulesBootstrap {
    private container: Container;

    private microframework: Microframework;

    /**
     * App constructor
     * @param container
     */
    constructor(container: Container) {
        this.container = container;
    }

    /**
     * Stop Framework Modules
     */
    public async gracefullyStop() {
        await this.microframework.shutdown();
    }

    /**
     * Init the framework modules
     */
    public async init() {
        return new Promise((resolve, reject) => {
            bootstrapMicroframework({
                config: {
                    debug: true,
                    showBootstrapTime: true
                },
                loaders: importClassesFromDirectories([`${__dirname}/../modules/*{.js,.ts}`])
            }).then((framework) => {
                this.microframework = framework;
                resolve(framework);
            }).catch((reason => {
                console.error("Application is crashed: " + reason);
                reject(reason);
            }));
        });
    }
}