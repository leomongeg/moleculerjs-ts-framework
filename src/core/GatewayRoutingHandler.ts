/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/20/19
 */
import { ServiceBroker }   from "moleculer";
import { routing }         from "../config/gateway.routes";
import { Container }       from "typedi";
import { BaseCoreService } from "./BaseCoreService";

export class GatewayRoutingHandler {

    private static _instance: GatewayRoutingHandler;

    private constructor() {
    }

    /**
     * Return a instance of the Actions Metadata Store
     *
     * @constructor
     * @return {ServiceMetadataStore}
     */
    public static get Instance(): GatewayRoutingHandler {
        return this._instance || (this._instance = new this());
    }

    /**
     * Publish the service routes to the api gateway
     *
     * @param {ServiceBroker} broker
     */
    public publishRoutes(broker: ServiceBroker) {
        const currentServiceName = Container.get(BaseCoreService.SERVICE_NAME_KEY);
        const gateways           = {} as any;
        for (const route of routing.routes) {
            const prototype = {...route} as any;
            delete prototype.aliases;
            delete prototype.gateway;
            prototype.aliases = {};

            if (prototype.service === undefined) {
                prototype.service = currentServiceName;
            }

            for (const alias in route.aliases) {
                const aliases            = route.aliases as any;
                prototype.aliases[alias] = this.buildAlias(aliases[alias], prototype);
            }

            prototype.id  = `${prototype.service}_${route.id}`;
            const gateway = (<any>route).publisher || routing.publisher;

            if (gateways[gateway] == undefined) {
                gateways[gateway] = [];
            }

            gateways[gateway].push(prototype);
        }

        for (const gateway in gateways) {
            broker.broadcast(gateway, gateways[gateway]);
        }
    }

    /**
     * Generate the route alias object for the Gateway.
     *
     * @param routeAlias
     * @param route
     */
    private buildAlias(routeAlias: any, route: any): any[] | string {
        if (typeof routeAlias == "string") {
            return `${route.service}.${routeAlias}`;
        }

        if (!(routeAlias instanceof Array)) {
            throw "Invalid Route Alias type allowed only string or array";
        }

        const aliasElements = [];
        for (const element of routeAlias)
            aliasElements.push(element);

        const action = aliasElements.pop();
        aliasElements.push(`${route.service}.${action}`);

        return aliasElements;
    }

    /**
     * Remove the service routes from the api gateway.
     *
     * @param broker
     */
    public unpublishRoutes(broker: ServiceBroker) {
        const services: any[]    = [];
        const currentServiceName = Container.get(BaseCoreService.SERVICE_NAME_KEY);
        console.log(currentServiceName);

        for (const route of routing.routes) {
            const _route = route as any;
            if (_route.service !== undefined && _route.service != currentServiceName)
                services.push({
                                  service    : _route.service,
                                  unpublisher: _route.unpublisher || routing.unpublisher
                              });
        }

        services.push({
                          service    : currentServiceName,
                          unpublisher: routing.unpublisher
                      });

        for (const route of services) {
            broker.broadcast(route.unpublisher, {
                service: route.service
            });
        }
    }

    /**
     * String representation of api gateway publishers event.
     *
     * @return {string[]}
     */
    public getGatewayPublisherEvents(): string[] {
        const publishers = [routing.publisherAvailableEvt];

        for (const route of routing.routes) {
            if (route.publisherAvailableEvt !== undefined)
                publishers.push(route.publisherAvailableEvt);
        }

        return publishers;
    }
}