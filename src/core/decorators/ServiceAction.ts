/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/16/19
 */

import { ServiceMetadataStore } from "../ServiceMetadataStore";
import slugify                  from "slugify";
import camelcase                from "camelcase";
import { ActionSchema }               from "moleculer";
import { BaseService } from "../BaseService";


/**
 * Options for the service action decorator see {Action} for more details.
 */
export interface ServiceActionOptions {
    name?: string;
    namespace?: string | boolean;

    [key: string]: any;
}

/**
 * Decorator to handle the registration of new controller service actions into the core service broker.
 *
 * @param opt
 * @constructor
 */
export function ServiceAction(opt: ServiceActionOptions = {}): (target: Object,
                                                                propertyName: string,
                                                                propertyDescriptor: PropertyDescriptor) => PropertyDescriptor {
    return (target: Object,
            propertyName: string,
            propertyDescriptor: PropertyDescriptor): PropertyDescriptor => {

        if (!propertyName.endsWith("Action"))
            throw "Actions method name should be named in the following way: `myAwesomeAction` your method name ending with the keyword `Action`";

        // Configure the service name and defaults Moleculer action props
        if (!opt.name || opt.name.trim() == "")
            opt.name = propertyName.replace("Action", "");
        if (opt.namespace === undefined || (typeof opt.namespace !== "string" && typeof opt.namespace !== "boolean"))
            opt.namespace = true;
        if (typeof opt.namespace === "boolean") {
            if (opt.namespace === false)
                opt.namespace = "";
            else
                opt.namespace = `${target.constructor.name.replace("Actions", "")}_`;
        } else {
            opt.namespace = `${slugify(opt.namespace)}_`;
        }
        if (opt.visibility === undefined) opt.visibility = "public";

        opt.handler = propertyDescriptor.value;
        opt.name    = camelcase(`${opt.namespace}${opt.name}`);

        delete opt.namespace;

        ServiceMetadataStore.Instance.addAction({name: opt.name, actionDefinition: opt as ActionSchema});

        return propertyDescriptor;
    };
}