/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/18/19
 */

import camelcase                             from "camelcase";
import slugify                               from "slugify";
import { ServiceMetadataStore }              from "../ServiceMetadataStore";
import { ServiceEvent, ServiceEventHandler } from "moleculer";

/**
 * Options for the service event decorator see {ServiceEvents} for more details.
 */
export interface ServiceEventOptions {
    name?: string;
    namespace?: string | boolean;

    [key: string]: any;
}

/**
 * Decorator to handle the registration of new service events into the core service broker.
 *
 * @param opt
 * @constructor
 */
export function ServiceEvent(opt: ServiceEventOptions = {}): (target: Object,
                                                              propertyName: string,
                                                              propertyDescriptor: PropertyDescriptor) => PropertyDescriptor {
    return (target: Object,
            propertyName: string,
            propertyDescriptor: PropertyDescriptor): PropertyDescriptor => {

        if (!propertyName.startsWith("on"))
            throw "Events method name should be named in the following way: `onMyAwesomeEvent` your method name starting with the keyword `on`";

        if (!propertyName.endsWith("Event"))
            throw "Events method name should be named in the following way: `onMyAwesomeEvent` your method name ending with the keyword `Action`";

        let hasName      = false;
        let hasNamespace = true;

        // Configure the service name and defaults Moleculer action props
        if (!opt.name || opt.name.trim() == "")
            opt.name = propertyName.replace("Event", "").replace("on", "");
        else
            hasName = true;
        if (opt.namespace === undefined || (typeof opt.namespace !== "string" && typeof opt.namespace !== "boolean"))
            opt.namespace = true;
        if (typeof opt.namespace === "boolean") {
            if (opt.namespace === false) {
                opt.namespace = "";
                hasNamespace  = false;
            } else
                opt.namespace = `${target.constructor.name.replace("Events", "")}_`;
        } else {
            opt.namespace = `${slugify(opt.namespace)}.`;
        }
        if (opt.visibility === undefined) opt.visibility = "public";

        opt.handler = propertyDescriptor.value;

        if (!hasName) {
            if (hasNamespace)
                opt.name = `${camelcase(opt.namespace)}.${camelcase(opt.name)}`;
            else
                opt.name = `${camelcase(opt.name)}`;
        } else {
            if (hasNamespace)
                opt.name = `${camelcase(opt.namespace)}.${opt.name}`;
        }


        delete opt.namespace;

        ServiceMetadataStore.Instance.addEvent({name: opt.name, eventDefinition: opt as ServiceEvent});

        return propertyDescriptor;
    };
}