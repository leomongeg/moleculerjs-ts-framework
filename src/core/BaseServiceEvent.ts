/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/19/19
 */

import { ServiceBroker }               from "moleculer";
import { getMongoManager, ObjectType } from "typeorm";

export abstract class BaseServiceEvent {
    protected broker: ServiceBroker;

    constructor(broker: ServiceBroker) {
        this.broker = broker;
    }

    /**
     * Get Custom repository from the MongoManagerInstance.
     * Example: const userRepo = this.getRepository(UserRepository);
     *
     * @param customRepository
     */
    protected getRepository<T>(customRepository: ObjectType<T>): T {
        return getMongoManager().getCustomRepository(customRepository);
    }
}