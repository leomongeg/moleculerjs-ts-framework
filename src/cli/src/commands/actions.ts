import { Command, flags } from "@oclif/command";
import cli from "cli-ux";
import * as fs from "fs";
// @ts-ignore
import { ucFirst } from "../../../util/commons";

export default class Actions extends Command {

  static description = "Create a new Action Class";

  static flags = {
    // Create new action
    create: flags.boolean({char: "c"}),
  };

  /**
   * Entry Point
   * ./cli/run
   */
  async run() {
    const {args, flags} = this.parse(Actions);
    const create = flags.create ?? undefined;

    if (create) {
      cli.log(" ### Create new Action Class ###");
      const actionClassName = await cli.prompt("What is your Action Class Name?");
      await this.createTmpActionCopy(actionClassName);
    }
  }

  /**
   * Creates a temporary class copy
   * @private
   */
  private async createTmpActionCopy(name: string) {
    const cname = ucFirst(name);

    if (await this.actionClassExists(cname)) {
      cli.error(`Action Class: ${cname}Actions.ts already exists`);
    }

    cli.action.start(`Creating Action class ${cname}Actions`, "initializing", {stdout: true});
    const destClassFile = `${__dirname}/assets/actions/tmp/${ucFirst(cname)}.ts`;
    fs.mkdirSync(`${__dirname}/assets/actions/tmp`); // // mkdirp(getDirName(path)
    fs.copyFileSync(`${__dirname}/assets/actions/Action_Class.tpl`, destClassFile);
    const data = fs.readFileSync(destClassFile, "utf8");

    if (!data) {
      cli.error("Error creating new class");
      cli.exit(1);
    }

    const result = data.replace(/{_class_name_}/g, cname);
    const clsDest = `${__dirname}/../../../actions/${cname}Actions.ts`;
    fs.writeFileSync(clsDest, result, "utf8");
    fs.rmSync(`${__dirname}/assets/actions/tmp/`, {recursive: true});
    cli.action.stop("Done");
  }

  /**
   * Validates if action class exists
   *
   * @param cname
   * @private
   */
  private async actionClassExists(cname: string) {
    try {
      fs.accessSync(`${__dirname}/../../../actions/${cname}Actions.ts`);
      return true;
    } catch {
      return false;
    }
  }
}
