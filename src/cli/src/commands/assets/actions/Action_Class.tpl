import { BaseServiceActions } from "../core/BaseServiceActions";
import { Context } from "moleculer";
import { ServiceAction } from "../core/decorators/ServiceAction";


export class {_class_name_}Actions extends BaseServiceActions {

    /**
     * Your first Action Method
     *
     * @param ctx
     * @private
     */
    @ServiceAction()
    private myFirstCallableAction(ctx: Context) {
        // Implementation Here
        return "Hello World";
    }
}
