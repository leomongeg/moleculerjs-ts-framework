/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/16/19
 */

import { Context }         from "moleculer";
import { BaseCoreService } from "../core/BaseCoreService";

class CoreHandler extends BaseCoreService {
    public static readonly SERVICE_NAME: string = "microservice-name";

    /**
     * Override of the Base Init method to initialize the Handler.
     */
    init() {
        this.name                    = CoreHandler.SERVICE_NAME;
        this.events["orm.connected"] = this.onORMConnected;

        /**
         * @TODO: This action setup is for demonstration please remove it
         */
        this.actions.myServiceAction = <any>{
            handler: this.myServiceAction
        };
    }

    /**
     * Demonstration in service handler action
     *
     * @TODO: This action setup is for demonstration please remove.
     *
     * @param ctx
     */
    private myServiceAction(ctx: Context) {
        return `Hello from ${CoreHandler.SERVICE_NAME}`;
    }

    /**
     * Event for ORM Connected.
     */
    private onORMConnected() {

    }
}

export = CoreHandler;