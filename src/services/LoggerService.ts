/**
 * This file can be copy to any service and used to add and send information to log service
 */
import { Container, Service } from "typedi";
import { ServiceBroker } from "moleculer";
import { IService } from "./interface/IService";
import { MongoObjectID } from "../util/commons";

/**
 * Error Level
 */
export enum Level {
    Info = "info",
    Error = "error"
}

export enum Source {
    Action = "action",
    Event = "event",
    Frontend = "frontend"
}

export enum Status {
    NEW = "new",
    FIXED = "fixed"
}

export enum Origin {
    META_INFO = 2,
    DISPATCH_LOG = 4,
    FOREACH = 4
}

/**
 * Store information used to reproduce errors
 */
export interface IHistory {
    // Browser information
    browser?: string;
    device?: string;
    // Function parameters
    method?: string;
    params?: any[];

    [key: string]: any; // Additional information
}

/**
 * Interface used to send information
 */
export interface ILogInformation {
    level: Level;
    label: string;
    information?: IHistory[];
    exception?: string;
    user?: MongoObjectID;
    source: Source;
    serviceName: string;
    stackTrace?: string;
    status: Status;
}

@Service()
export class LoggerService extends IService {

    /**
     * Creates the basic structure of Log
     */
    createStructure(): any {
        return {
            exception: undefined,
            information: [],
            label: "",
            level: Level.Info,
            serviceName: "",
            source: Source.Action,
            stackTrace: undefined,
            status: Status.NEW,
            user: undefined
        } as ILogInformation;
    }

    /**
     * Set Error information
     * @param label
     * @param source
     * @param user
     */
    setErrorInformation(label: string, source: Source, error?: Error, user?: string) {
        const message = this.createStructure();
        message.label = label;
        message.source = source;

        if (error !== undefined) {
            message.exception = error.message;
            message.stackTrace = error.stack;
        }

        if (user !== undefined) {
            message.user = user;
        }

        return message;
    }

    /**
     * Log information into the service
     * @param label message from developer
     * @param source source from the message
     * @param argumentList list of arguments in the function where the error was thrown
     * @param user
     * @param error
     */
    dispatchLog(label: string, source: Source, argumentList?: IArguments, user?: string, error?: Error) {
        const message = this.prepareMessage(label, source, argumentList, user, error);
        message.level = Level.Info;
        Container.get(ServiceBroker).emit("log.info", message);
    }

    /**
     * Create a new Exception
     * @param label message from developer
     * @param source source from the message
     * @param argumentList list of arguments in the function where the error was thrown
     * @param user
     * @param error
     */
    dispatchError(message: ILogInformation) {
        message.level = Level.Error;
        Container.get(ServiceBroker).emit("log.info", message);
    }

    /**
     * Creates a message with the error/info
     * @param label message from developer
     * @param source source from the message
     * @param argumentList list of arguments in the function where the error was thrown
     * @param user
     * @param error
     */
    private prepareMessage(label: string, source: Source, argumentList: IArguments | any[] | undefined, user: string | undefined, error: Error | undefined) {
        let message = this.setErrorInformation(label, source, error, user);
        // Add's information to replicate the error
        if (argumentList !== undefined) {
            message = this.addMethodInfo(argumentList, message, Origin.DISPATCH_LOG);
        }
        return message;
    }

    /**
     * Creates a method call metadata
     */
    addMethodInfo(argument: IArguments | any[], message: ILogInformation, level: Origin = Origin.META_INFO) {
        const e = new Error("dummy");
        if (!e.stack) return;
        const functionName = e.stack
            .split("\n")[level]
            // " at functionName ( ..." => "functionName"
            .replace(/^\s+at\s+(.+?)\s.+/g, "$1");
        if (!message.information)
            message["information"] = [];
        message.information.push(
            {
                method: functionName,
                params: Array.from(argument) // Convert to a array
            }
        );

        return message;
    }
}