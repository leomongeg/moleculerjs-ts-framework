/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/3/18
 */
import { getMongoManager, ObjectType } from "typeorm";
import { Container } from "typedi";
import { ServiceBroker } from "moleculer";

export class IService {

    /**
     * Use this function to alert the user how to use or define the access token in the env file.
     */
    usageTip ?: () => void;

    /**
     * Use this function to treat the result in the way you need
     * @param resultset
     * @param call
     * @returns {any}
     */
    parseResult ?: (resultset: any, call: any) => any;

    /**
     * Get Custom repository from the MongoManagerInstance.
     * Example: const userRepo = this.getRepository(UserRepository);
     *
     * @param customRepository
     */
    protected getRepository<T>(customRepository: ObjectType<T>): T {
        return getMongoManager().getCustomRepository(customRepository);
    }

    /**
     * Make a call to another microservice
     * @param {string} call
     * @param args
     * @returns {Promise<any>}
     */
    protected makeBrokerCall(call: string, args: any) {
        const broker = Container.get(ServiceBroker);
        return broker.call(call, args);
    }
}