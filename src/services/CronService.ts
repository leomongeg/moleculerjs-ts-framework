import { Container, Service } from "typedi";
import { IService } from "./interface/IService";
import { ServiceBroker } from "moleculer";

const cron = require("moleculer-cron");

@Service()
export class CronService extends IService {

    /**
     * Creates a cronjob
     * @param {string} name
     * @param {string} cronTime
     * @param {string} action
     * @param {string} timeZone
     */
    public createCronJob(name: string, cronTime: string = "* * * * *", action: string,  timeZone: string = "America/Costa_Rica") {
        const broker = Container.get(ServiceBroker);

        /* tslint:disable */
        broker.createService({
            name: name,
            mixins: [cron],
            crons: [
                {
                    name: name,
                    cronTime: cronTime,
                    onTick: function() {
                        console.log("Trying to execute " + action);
                        this.makeBrokerCall(action, {});
                        console.log("Executed " + name);
                    },
                    runOnInit: function() {
                        console.log(name + " is created");
                    },
                    // manualStart: true,
                    timeZone: "America/Costa_Rica"
                }
            ]
        });
        /* tslint:enable */
    }
    /**
     * How to use the Service
     */
    usageTip = (): void => {
        console.log("~~~~~~~~~~");
        console.log("Cron Service");
        console.log("~~~~~~~~~~");
    };
}