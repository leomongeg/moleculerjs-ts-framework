/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/16/19
 */

import { ServiceAction }      from "../core/decorators/ServiceAction";
import { Context }            from "moleculer";
import { BaseServiceActions } from "../core/BaseServiceActions";
import { __ } from "i18n";

/**
 * @TODO: This is a demonstration ServiceAction Class please DELETE.
 */
export class AcmeActions extends BaseServiceActions {

    /**
     * Add Action. Perform add operation over the param foo and bar (foo + bar) = number
     *
     * @param ctx
     */
    @ServiceAction({
        params: {
            bar: {type: "number", convert: true},
            foo: {type: "number", convert: true}
        },
        visibility: "published"
    })
    private addAction(ctx: Context) {
        const params = <any>ctx.params;
        const total  = (params.bar + params.foo);
        __("welcome", {name: params.name});
        this.broker.emit("acme.addResult", {answer: total});
        return {total: total};
    }

    /**
     * Test Action
     *
     * @param ctx
     * @private
     */
    @ServiceAction({})
    private testAction(ctx: Context) {
        console.log(this);
        return "Hello Test";
    }
}