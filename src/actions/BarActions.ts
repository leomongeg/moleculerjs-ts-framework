import { BaseServiceActions } from "../core/BaseServiceActions";
import { Context } from "moleculer";
import { ServiceAction } from "../core/decorators/ServiceAction";
import { getRepository } from "typeorm";
import { Product } from "../entities/Product";


export class BarActions extends BaseServiceActions {
    /**
     * find by id
     * @param ctx
     * @private
     */
    @ServiceAction({cache: true})
    private findByIdAction(ctx: Context) {
        console.log("Llego");
        const prodRepo = getRepository(Product);
        return prodRepo.find();
        // return { hola: "mundo"};
    }
}