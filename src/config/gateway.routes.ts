/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/20/19
 */

import { GatewayRouting } from "../core/types/GatewayRouting";


export const routing: GatewayRouting = {
    publisher            : "gateway.routing.publishRoutes",
    publisherAvailableEvt: "gateway.routing.publisherAvailable",
    routes               : [
        {
            aliases      : {

            },
            authorization: true,
            // Use bodyparser module
            bodyParsers  : {
                json      : true,
                urlencoded: {extended: true}
            },
            id           : "api",
            path           : "/api",
        }
    ],
    unpublisher          : "gateway.routing.unpublishRoutes",
};