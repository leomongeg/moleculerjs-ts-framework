import {
    BaseEntity,
    Column,
    CreateDateColumn,
    Entity,
    ObjectID,
    ObjectIdColumn, UpdateDateColumn
} from "typeorm";

@Entity()
export class Product extends BaseEntity {
    @ObjectIdColumn() id?: ObjectID;

    @Column()
    public name: string;

    @CreateDateColumn()
    public createdAt: Date;

    @UpdateDateColumn()
    public updatedAt: Date;

}