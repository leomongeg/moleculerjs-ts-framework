/**
 * Merges information using fields in destination. don't create new fields
 * @example: destination = EntityMerge.forceUpdateFields(origin, destination, ["id", "name"])
 * @param origin
 * @param destination
 */
export function forceUpdateFields(origin: any, destination: any, attributes: Array<string>) {

    attributes.forEach(function (attribute: string) {
        // @ts-ignore
        if (typeof origin[attribute] !== "undefined" && destination[attribute] !== origin[attribute]) {
            // @ts-ignore
            destination[attribute] = origin[attribute];
        }
        else {
            destination[attribute] = "";
        }
    });
    return destination;
}

/**
 * Merge information into a destination, it creates not existing fields
 * @example: let updatedFields = EntityMerge.merge(origin, destination);
 * @param origin
 * @param destination
 */
export function merge(origin: any, destination: any, force: boolean = false) {
    let fieldsUpdated = 0;
    for (const attribute in origin) {
        // Process sub objects validating fields
        if (typeof origin[attribute] === "object") {
            if (destination[attribute] === undefined) {
                destination[attribute] = {}; // Initialize field
            }
            fieldsUpdated = fieldsUpdated + merge(origin[attribute], destination[attribute]);
        }
        else if ((destination[attribute] !== origin[attribute]
            && origin[attribute] !== "" && origin[attribute] !== undefined) || force) {
            destination[attribute] = origin[attribute];
            fieldsUpdated++;
        }
    }
    return fieldsUpdated;
}

/**
 * Clone a object into a new instance
 * @param origin
 */
export function clone<T>(origin: T) {
    const cloned = {} as T;
    merge(origin, cloned);
    return cloned;
}