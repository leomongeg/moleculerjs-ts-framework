/**
 * Get the configuration from the parameter service
 * @param parameter
 */
import { Container } from "typedi";
import { ServiceBroker } from "moleculer";
import { Messages } from "./messages";

/**
 * Get the configuration from configuration storage
 * @param parameter
 */
export async function getParameter(parameter: string, defaultValue?: any) {
    try {
        // get the value from the service
        const value = await Container.get(ServiceBroker)
            .call("configuration_store.parameterGetValue", {key: parameter});
        if (value !== undefined) {
            return value;
        }
    } catch (e) {
        // todo handle error
        console.log(e);
    }

    return getDefaultValue(defaultValue);
}

/**
 * Verify if the default value is a function and returns the processed value
 * This allow to process information from environment only when is necesary
 * Example split a string
 * @param defaultValue
 */
function getDefaultValue(defaultValue: any) {
    // Resolve a function defautl value
    if (defaultValue instanceof Function) {
        return defaultValue();
    }
    return defaultValue;
}

/**
 * Create a parameter if is not register
 * @param key
 * @param value
 */
export async function setWhenUndefined(key: string, value: any) {
    const parameter = await getParameter(key, undefined);
    if (parameter === undefined) {
        const result = await setParameter(key, getDefaultValue(value));
        if (!(result instanceof Error)) {
            return getDefaultValue(value);
        }
    }

    return Error(Messages.INVALID_PARAMETER);
}

/**
 * Create a parameter
 * @param key
 * @param value
 */
export async function setParameter(key: string, value: any) {
    try {
        // Create the parameter in the service
        const result = await Container.get(ServiceBroker)
            .call("configuration_store.parameterAdd", {
                key: key,
                value: getDefaultValue(value)
            });
        // Return the value when the parameter is created
        if ((result as any).status === true) {
            return getDefaultValue(value);
        }
    } catch (e) {
        // Todo handle error
        console.log(e);
    }
    // Return error when cant save the parameter
    return Error(Messages.INVALID_PARAMETER);
}