import * as path from "path";
import * as fs from "fs";
import { BaseServiceActions } from "../core/BaseServiceActions";
import glob from "glob";

/**
 * Loads all exported classes from the given directory.
 */
export function importClassesFromDirectories(directories: string[], formats = [".js", ".cjs", ".ts"]): any[] {

    const logLevel = "info";
    const classesNotFoundMessage = "No classes were found using the provided glob pattern: ";
    const classesFoundMessage = "All classes found using provided glob pattern";

    /**
     * Load File Classes
     * @param exported
     * @param allLoaded
     */
    function loadFileClasses(exported: any, allLoaded: Function[]) {
        if (typeof exported === "function" || exported instanceof BaseServiceActions) {
            allLoaded.push(exported);

        } else if (Array.isArray(exported)) {
            exported.forEach((i: any) => loadFileClasses(i, allLoaded));

        } else if (typeof exported === "object" && exported !== null) {
            Object.keys(exported).forEach(key => loadFileClasses(exported[key], allLoaded));

        }
        return allLoaded;
    }

    const allFiles = directories.reduce((allDirs, dir) => {
        return allDirs.concat(glob.sync(PlatformTools.pathNormalize(dir)));
    }, [] as string[]);

    // if (directories.length > 0 && allFiles.length === 0) {
    //     logger.log(logLevel, `${classesNotFoundMessage} "${directories}"`);
    // } else if (allFiles.length > 0) {
    //     logger.log(logLevel, `${classesFoundMessage} "${directories}" : "${allFiles}"`);
    // }
    const dirs = allFiles
        .filter(file => {
            const dtsExtension = file.substring(file.length - 5, file.length);
            return formats.indexOf(PlatformTools.pathExtname(file)) !== -1 && dtsExtension !== ".d.ts";
        })
        .map(file => require(PlatformTools.pathResolve(file)));

    return loadFileClasses(dirs, []);
}



/**
 * Platform-specific tools.
 * https://github.com/typeorm/typeorm/blob/f85f436f51fb000cd9959b44e8d7a79bf0cd10ab/src/platform/PlatformTools.ts#L14
 */
export class PlatformTools {

    /**
     * Normalizes given path. Does "path.normalize".
     */
    static pathNormalize(pathStr: string): string {
        return path.normalize(pathStr);
    }

    /**
     * Gets file extension. Does "path.extname".
     */
    static pathExtname(pathStr: string): string {
        return path.extname(pathStr);
    }

    /**
     * Resolved given path. Does "path.resolve".
     */
    static pathResolve(pathStr: string): string {
        return path.resolve(pathStr);
    }

    /**
     * Synchronously checks if file exist. Does "fs.existsSync".
     */
    static fileExist(pathStr: string): boolean {
        return fs.existsSync(pathStr);
    }

    /**
     * readFileSync
     * @param filename
     */
    static readFileSync(filename: string): Buffer {
        return fs.readFileSync(filename);
    }

    /**
     * appendFileSync
     * @param filename
     * @param data
     */
    static appendFileSync(filename: string, data: any): void {
        fs.appendFileSync(filename, data);
    }

    /**
     * projectRootDir
     */
    static projectRootDir(): string {
        // console.log(path.parse(require.main.filename).dir);
        // console.log(path.dirname(""));
        // fs.readdir((path.dirname("") + "/dist/actions"), (err, files) => {
        //     files.forEach(file => {
        //         console.log(file);
        //     });
        // });
        return path.dirname("");
    }

}