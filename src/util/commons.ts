/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 02/18/19
 */

import { ServiceBroker } from "moleculer";
import { ObjectID } from "typeorm";

/**
 * Type Alias for the TypeORM ObjectID
 */
export type MongoObjectID = ObjectID;

/**
 * Convert all the first character of the given string uppercase.
 *
 * @param text
 */
export function ucFirst(text: string) {
    text = text.toLowerCase()
               .split(" ")
               .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
               .join(" ");

    return text;
}

/**
 * Search if the given service caller is available.
 *
 * @param caller
 * @param broker
 */
export function serviceIsAvailable(caller: string, broker: ServiceBroker): boolean {
    const services = (broker.registry as any).getActionList({onlyLocal: false});

    const available = services.find((element: any) => {
        return element.name == caller;
    });

    return !!available;
}

/**
 * Converts string or given value into boolean representation.
 *
 * @param value
 */
export function getBoolean(value: any): boolean {
    switch (value) {
        case true:
        case "true":
        case 1:
        case "1":
        case "on":
        case "yes":
            return true;
        default:
            return false;
    }
}

/**
 * Convert the ref enum into a string of all the options.
 *
 * @param enumDef
 * @param separator
 * @return {string}
 */
export function enumImplode(enumDef: any, separator: string = ","): string {
    return Object.keys(enumDef)
                 .map((property) => `${property}=${enumDef[property]}`)
                 .join(separator);
}