import { ServiceBroker } from "moleculer";
import { Container } from "typedi";
import "reflect-metadata";
import { MicroserviceModulesBootstrap } from "../core/MicroserviceModulesBootstrap";

export = async function () {
    const moduleLoader = new MicroserviceModulesBootstrap(Container);
    await moduleLoader.init();

    return {
        cacher:     "Memory",
        logFormatter: "short",
        logLevel    : "info",
        logger      : true,
        metrics     : true,
        namespace   : "",
        transporter : process.env.TRANSPORTER,
        created(broker: ServiceBroker) {
            Container.set(ServiceBroker, broker);
        },
        started(broker: ServiceBroker) {
            // InitMongoConnection.createMongoConnection(broker).then(success => {
            //     GatewayRoutingHandler.Instance.publishRoutes(broker);
            // });

            console.log("LLEGO STSRTYED");
            // const cronService = Container.get(CronService);
            // Every 2 minutes
            // cronService.createCronJob("retryAppointments", "*/2 * * * *", "appointments.appointmentRetry");
        },
        async stopped(broker: ServiceBroker) {
            await moduleLoader.gracefullyStop();
        }
    };
};