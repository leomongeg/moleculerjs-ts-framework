# Base Microservices with MoleculerJS - TypeScript Project

## Motivation


This base project for microservices with Moleculerjs and TypeScript, helps to build microservices using techniques like

Dependency Injection and other well know development patterns like Repositories and full object-oriented

### Global packages

* `moleculer-repl`: 0.6.1 -> `npm -g moleculer-repl@0.6.1`
* `moleculer-cli`: 0.6.6 -> `npm -g moleculer-cli@0.6.6`

### Project stack.


The main idea is to use a well-structured class for our microservice instead use a plain object how the documentation
 suggests. And another important point is to define a well-structured project stack that easily allows developers
to group the business logic into Dependency Injection Services and Repositories for the data access Layer.


### Utils

#### entity\base\BaseEntity
This base gives a merge function to a entity. To do.. Use [Trait pattern](https://github.com/typescript-plus/mixin-decorator/blob/master/spec/include/nested_mixins.spec.ts)

#### Merge and Clone
These 3 functions allow to copy attributes from one object to another.
- merge: copy all information from a class to another, it creates fields in the destination class that dont exists.
- forceUpdateFields: copy a list of fields from origin object to destination.
- clone: this function copy a object and return a new one with the same information.

### Paginator

This is a recommended standard to return to frontend and contains the number of elements that match the search,
list of elements, number of pages, first and last (when is the last or first page).

It returns a Object with this structure:

```typescript
class Paginator<T> {
     count: number; // Total number of items
     first: boolean; // Is first page
     items: Array<T>; // List of items
     last: boolean; // Is last page
     pages: number; // Number of pages
 }
```

There is two ways to make a pagination

- BaseRepository.list(params: any)

Base repository has a function call "list", this function find all items that match rules in the param.

Obligatory parameters: 
take: number of items to show with a max of 1000 elements.
skip: page to show it starts from 1, the real skip is calculated using take's value.

**Example**
private async findByOwnerAction(ctx: Context) {
        const paginator = await this.getRepository(VehicleRepository).list(ctx.params);
        return this.success(paginator);
    }

        
- protected function BaseRepository.createPaginator

Other way to paginate is using the protected function createPagination(params, countPromise, findPromise) can be used to create pagination in custom
querys

**Parameters used:**

params: object with search parameters (take and skip are needed).
countPromise: a Promise (async method) to calculate number of elements.
findPromise: a Promise (async method) to search elements in the current page.

**Example**
```typescript
const findPromise = this.getRepository(VehicleRepository).list(queryParamsFind);
const countPromise = this.getRepository(VehicleRepository).countVehicles(queryParamsCount);

// Dispatch error
const paginatorResponse = await this.createPagination(params, countPromise, findPromise);

// Dispatch info
loggerInstance.dispatchLog("Store message as info", Source.Event, arguments, log.getUser());
```

The last option is create a pagination is from the Paginator's static function
```typescript
const paginatorResponse = Paginator.createInstance({
    count: count,
    first: params.skip === undefined || params.skip === 1,
    items: await findPromise,
    last: numberOfPages <= params.skip,
    pages: numberOfPages
});
```

Paginator can create a empty pagination
```typescript
const emtpy = Paginator.empty<any>();
```
---
### Configuration
This helper allow to read configuration from configuration_storage.

**getParameter**

Get a parameter and if there is a error or is empty returns a default value.

The value can be of any Type.
```typescript
// Basic Example
const parameter = getParameter("PARAMETER_NAME_IN_STORAGE", "DEFAULT_VALUE");
// getParameter with a function
const paramete2 = getParameter("email", () => {
    return process.env.email.split(",");
});
```

**setParameter**

Allow to set a parameter to a given value.
```typescript
const result = await setParameter(key, value);
```

**setWhenUndefine**
This function was made to set the parameters the first time the service runs. When the value already exists it don't assign the value.

---
### Report Error
 
Base service used to report errors and store information in the logService (micoservice-log). All messages are send using event (keep tottaly assynchronously).

Usage:
```typescript
const loggerService = Container.get(LoggerService);
let message = loggerService.setErrorInformation("Test error",
    Source.Action,
    new Error("Test error"),
    "5eb0aa3f24771159e2f7b77d",
);
message = loggerService.addMethodInfo(arguments, message, Origin.META_INFO);

loggerService.dispatchError(message);
```
#### setErrorInformation: 
Init a message object with a label

Parameters:
- Label or message
- source (action, event or frontend)
- the error thrown by the system (optional)
- user Id (optional)

Returns: object where the log entry is stored

#### addMethodInfo: 

Add information of the current method

Parameters:
- arguments (javascript list of arguments from the function)
- message: object where the log entry is stored
- level: amount of lines to get function name (Origin.META_INFO, Origin.DISPATCH_LOG, Origin.FOREACH)

Returns: 
    message: object where the log entry is stored
    
#### dispatchError
Sends the message to the log service

Parameters:
- message: object where the log entry is stored

#### dispatchLog
Sends a log entry to the log service

Parameters:
- Label or message
- source (action, event or frontend)
- arguments (javascript list of arguments from the function)
- user Id (optional)
- the error thrown by the system (optional)


---

### Get Started


In order to understand the project stack is important to understand:

- [What is a Microservice?](https://microservices.io/)

- [What is Moleculer JS?](https://moleculer.services/docs)

- [What is Dependency Injection?]((docs/dependency_injection.md))

- [What is the Repository pattern?](docs/model_repositories.md)


Once you have the basic knowledge basis you are able to understand the [project stack](docs/project_stack.md). So please, continue to the next

step that is [Understanding the Project Stack](docs/project_stack.md) and [THI Microservice Frameworks](docs/framework.md)

---

### Documentation

Please read this docs in order to start development:

- [Dependency Injection](docs/dependency_injection.md)

- [Create model files and Repository pattern](docs/model_repositories.md)

- [Microservices io](https://microservices.io/)

- [Moleculer JS](https://moleculer.services/docs)

- [THI Microservice Frameworks](docs/framework.md)

- [Publish Routes to API Gateway](docs/api_gateway.md)

- [Unit Testing](docs/unit_testing.md)

---

### Get Started

- [Docker For Develop](docker/README.md)

- [THI Microservice Frameworks](docs/framework.md)

---
