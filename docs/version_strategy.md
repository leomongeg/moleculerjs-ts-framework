# Semantic Versioning

To keep Ecosystem healthy, reliable, and secure, every time that a significant change is made, updates to an package or
Microservice, we recommend publishing a new version of the package or Microservice with an updated [Tag version](https://git-scm.com/book/en/v2/Git-Basics-Tagging) 
number in repository and the package.json file that follows the semantic versioning spec. Following [the semantic 
versioning spec](https://semver.org/) helps other developers who depend on your code understand the extent of changes 
in a given version, and adjust their own code if necessary.


## Semantic Versioning 2.0.0

Given a version number MAJOR.MINOR.PATCH, increment the:

    MAJOR version when you make incompatible API changes, such as an patform upgrade, a build system upgrade, etc.
    MINOR version when you add functionality in a backwards-compatible manner, and for the first new tag after a production deployment.
    PATCH version when you make backwards-compatible bug fixes, for any tags that update a new minor prior to a production deployment.

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

Example:

    Tag 4.50.3 deployed to production
    Next tag deployed to staging would be 4.51.0
    Unless 4.51.0 is deployed to production, the next tag deployed to staging would be 4.51.1
    Unless 4.51.1 is deployed to production, the next tag deployed to staging would be 4.51.2
    ... repeat until a 4.51.x tag is deployed to production
    Next tag deployed to staging would be 4.52.0

## Incrementing semantic versions in published packages

To help developers who rely on your code, we recommend starting your package version at 1.0.0 and incrementing as follows:


|           Code status            |    Stage      | Rule                                                               |Example version|
|:--------------------------------:|:-------------:|:------------------------------------------------------------------:|:-----:|
| Development - UAT releases       | Development   | Start with 0.0.0                                                   | 0.0.1 |
| First release                    | New product   | Start with 1.0.0                                                   | 1.0.0 |
| Backward compatible bug fixes    | Patch release | Increment the third digit                                          | 1.0.1 |
| Backward compatible new features | Minor release | Increment the first digit and reset middle and last digits to zero | 2.0.0 |


## How Branches are handled

The descriptions of how commits and branches are versioned can be considered a type of pseudopod. With that in mind 
there are a few common "variables" that we will refer to:

- targetBranch => the branch we are targeting
- targetCommit => the commit we are targeting on targetbranch


### Master branch

Commits on master will always be a merge commit (Either from a hotfix or a release branch) or a tag. As such we can 
simply take the commit message or tag message.

If we try to build from a commit that is no merge and no tag then assume 0.1.0

mergeVersion => the SemVer extracted from targetCommit.Message

- major: `mergeVersion.Major`
- minor: `mergeVersion.Minor`
- patch: `mergeVersion.Patch`
- pre-release: 0 (perhaps count ahead commits later)
- stability: final

Optional Tags (only when transitioning existing repository): * TagOnHeadCommit.Name={semver} => overrides the version 
to be {semver}


### Develop branch

targetCommitDate => the date of the targetCommit masterVersionCommit => the first version (merge commit or SemVer tag) 
on master that is older than the targetCommitDate masterMergeVersion => the SemVer extracted from masterVersionCommit.Message

- major: `masterMergeVersion.Major`
- minor: `masterMergeVersion.Minor` + 1 (0 if the override above is used)
- patch: 0
- pre-release: `alpha.{n}` where n = how many commits `develop` is in front of masterVersionCommit.Date ('0' padded to 4 characters)

### Release branches

- Should branch off from: develop
- Must merge back into: develop and master
- Branch naming convention: release-{n} eg release-1.2

releaseVersion => the SemVer extracted from targetBranch.Name releaseTag => the first version tag placed on the branch. 
Note that at least one version tag is required on the branch. The recommended initial tag is {releaseVersion}.0-alpha1. 
So for a branch named release-1.2 the recommended tag would be 1.2.0-alpha1

- major: mergeVersion.Major
- minor: mergeVersion.Minor
- patch: 0
- pre-release: {releaseTag.preRelease}.{n} where n = 1 + the number of commits since releaseTag.

So on a branch named release-1.2 with a tag 1.2.0-alpha1 and 4 commits after that tag the version would be 1.2.0-alpha1.4


### Hotfix branches

Named: hotfix-{versionNumber} eg hotfix-1.2

branchVersion => the SemVer extracted from targetBranch.Name

- major: mergeVersion.Major
- minor: mergeVersion.Minor
- patch: mergeVersion.Patch
- pre-release: beta{n} where n = number of commits on branch ('0' padded to 4 characters)

### Pull-request branches

Should branch off from: develop Must merge back into: develop Branch naming convention: anything except master, develop, 
release-{n}, or hotfix-{n}. Canonical branch name contains /pull/.

- major: masterMergeVersion.Major
- minor: masterMergeVersion.Minor + 1 (0 if the override above is used)
- patch: 0
- pre-release: alpha.pull{n} where n = the pull request number ('0' padded to 4 characters)

### Nightly Builds

develop, feature and pull-request builds are considered nightly builds and as such are not in strict adherence to SemVer.