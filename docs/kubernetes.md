
  
# Base Microservices Deploy Kubernetes 
  
### [Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)  is an open-source system for automating deployment, scaling, and management of containerized applications.  
  
This is a base project to create a microservice in Kubernetes. It was create a base of:  
  
 - [ ] [Node.js and MongoDB on Kubernetes](https://github.com/kubernetes/examples/tree/master/staging/nodesjs-mongodb)   

 - [ ] [CI/CD using CircleCI and Google Kubernetes Engine (GKE)](https://medium.com/@admm/ci-cd-using-circleci-and-google-kubernetes-engine-gke-7ed3a5ad57e)

 - [ ] [Azure Provider: Authenticating using a Service Principal](https://www.terraform.io/docs/providers/azurerm/authenticating_via_service_principal.html)
 
 - [ ] [Container Training](https://container.training/) Recommended Read
    
  
![kubernetes](https://k8s2d.container.training/images/k8s-arch1.png)  
 
  
## 1 How To Run  
  
### 1.1 Prerequisites  

Ensure the following dependencies are already fulfilled on your host Linux/Windows/Mac Workstation/Laptop:  

#### 1.1.1 Get the credentials of azure:

        - AZURE_DOCKER_REGISTRY_PROJECT_ID  
        - AZURE_USERNAME
        - AZURE_PASSWORD
        - AZURE_SUBSCRIPTION_ID
        - AZURE_SERVICE_TENANT  
        - AZURE_CLIENT_ID  
        - AZURE_CLIENT_SECRET  
        - AZURE_TENANT_ID  
        - AZURE_GROUP  
        - AZURE_PROJECT

**AZURE_DOCKER_REGISTRY_PROJECT_ID:**  Login server

**AZURE_USERNAME:**  Username of Docker Registry

**AZURE_USERNAME:**  Password of Docker Registry

**AZURE_SUBSCRIPTION_ID:** Subscription ID of Kubernetes service

**AZURE_GROUP:** Resource group Name

**AZURE_PROJECT:** Kubernetes service Name

For get the **AZURE_SERVICE_TENANT, AZURE_CLIENT_ID, AZURE_CLIENT_SECRET, AZURE_TENANT_ID** you need run

    az account set --subscription="${AZURE_SUBSCRIPTION_ID}"
    az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/${AZURE_SUBSCRIPTION_ID}"

The Azure Cli return a Json, where you get the values pending: 

    {
      "appId": "${AZURE_CLIENT_ID}",
      "displayName": "azure-cli-2018-10-08-21-41-28",
      "name": "http://azure-cli-2018-10-08-21-41-28",
      "password": "${AZURE_CLIENT_SECRET}",
      "tenant": "${AZURE_TENANT_ID}"
    }
    
#### 1.1.2 [Circle CI Local](https://circleci.com/docs/2.0/local-cli/)
Install Circle CI Local: https://circleci.com/docs/2.0/local-cli/

  
### 1.2 Main Deployment Steps  
  
    
### 1.2.1 Deploy Microservice JS 
  
To deploy the Node JS Service via a Circle CI, you need edit the file:  

       ${Project}/.circleci/config.yml

Change the Job **build** with the the credentials of azure (From Pprerequisites) and Kubernetes and Docker Names:  

    - KUBE_NAMESPACE # Production | Development | QA
    - CONTAINER_NAME # Name of container image on docker regestry
    - PROJECT_NAME # Name Microservice
    - PORT_IMAGE # Port expose of Microservice
    - PORT_NAME # Name of port expose of Microservice, this can't exceds the 15 characters


Before the all the change on config file of Circle CI you need validate the file with this command on the root project path:

    $ circleci config validate
  
If the validate is correct then you run:

    $ circleci local execute --job build


### 1.2.2 Delete services

If you need delete the services from Kubernetes you need:

#### Connect to the Kubernetes cluster

To manage a Kubernetes cluster, use kubectl, the Kubernetes command-line client.

If you're using Azure Cloud Shell, kubectl is already installed. If you want to install it locally, use the az aks install-cli command.

    $ az aks install-cli
    
To configure kubectl to connect to your Kubernetes cluster, use the az aks get-credentials command. This step downloads credentials and configures the Kubernetes CLI to use them.


    $ az aks get-credentials --resource-group <group> --name <name-project>

#### Delete the service:

    $ cd kubernetes/scripts-node && ./delete_service.sh <PROJECT_NAME> <NAMESPACE>
    
    

  
  
  