# THI Corporate Microservices Framework

The THI Corporate Microservice Framework is a top implementation of the lightweight [moleculer microservice framework](https://moleculer.services/) for NodeJS.

We worked to develop a more robust, easy to use, well structured and powerful framework to easy build microservices using advanced tools
like Dependency Injection, and ORM.

## Get Started

In order to start the development clone this base project:

```bash
$ git clone https://bitbucket.criticalmass.com/scm/ep/base-microservice-framework.git
```

Then initialize you development environment: 

- Remove the current git instance:

```bash
$ rm -rf .git
```

- Initialize your local repository

```bash
$ git init
```

- Create your .env file copying the .env.example file and edit with your local configuration

```bash
$ cp ./.env.example ./.env
```

Important, the microservice needs a [transporter](https://moleculer.services/docs/0.13/networking.html) the default 
transported defined in the .env.example file is RabbitMQ but you can change for any of the available transporters. You
can remove the `TRANSPORTER` property form your .env file and the system use TCP as default transporter. 

- Install the project dependencies

```bash
$ npm i
```

- To start the development mode run

```bash
$ npm run debug
```

If you want you can use Docker for development. Please checkout the [Docker Setup](docker.md) docs for more details.  


## Service Handlers

The base project provide to you a pre configured service handler called [core.service.ts](../src/handlers/core.service.ts)
in order to start the development find the constant `SERVICE_NAME` and replace the value with the name of your
microservice, for example `slack`. Once that you that you are able to see the new service in the service broker. 

To test your microservice use [moleculer cli](https://moleculer.services/docs/0.12/moleculer-cli.html):

```bash
mol $ moleculer connect
```

Then to list your services execute `$ services` and you can see the list of registered microservices in your services broker:

![picture](assets/services.png)


As you can see the [core.service.ts](../src/handlers/core.service.ts) has a demonstration ServiceAction called `myServiceAction` 
if you want you can create your own service actions in the core.service.ts but there is a better way to create services actions.

## Create Service Actions

In the project structure, you can see a directory called actions, this directory contains all your project microservice Actions.
To create a new Service Action you only need to create a class called as following `NamespaceActions` and extends your 
from [BaseServiceActions](../src/core/BaseServiceActions.ts), for more details see the demonstration Service 
Actions class [AcmeActions](../src/actions/AcmeActions.ts)

Once that you already created the class you can start creating you service actions as following:

- Import the service action decorator:

```typescript
import { ServiceAction } from "../core/decorators/ServiceAction";
```

- Create your action method and initialize the decorator:

```typescript
export class AcmeActions extends BaseServiceActions {
    
    @ServiceAction()
    private findByEmailAction(ctx: Context): any {
        
    }
}
```

Then, go to the [actions loader](../src/actions/loader/actionsLoader.ts) file and register your class. And that's it 
your actions are registered in your service, to review the registered actions, execute `$ actions` in the moleculer cli:

![picture](assets/actions.png)

 
As you can see the action is defined with the class name prefix `acme`, followed by the method name without the keyword Action,
this is the default behavior but is totally overridable with the ServiceAction decorator options.

#### Service action options

The decorator ServiceAction allows you to configure your service action for more details of the service action properties,
please see the [MoleculerActions docs](https://moleculer.services/docs/0.13/actions.html). As mentioned before, you
can change how the core register your action for example you can avoid the namespace and add a custom name for example:


```typescript
    @ServiceAction({
        name: "findUserByEmailAddress",
        namespace: false
    })
    private findByEmailAction(ctx: Context): any {
        
    }
```

To test call in the moleculer cli the actions command `$ actions`:


![picture](assets/actions_rename.png)

As you can see the acme prefix disappears and the action name equals to the value defined by the name parameter in the decorator. The 
namespace parameter by default is true and by default use the action class name as a namespace, if  the namespace value is string,
the decorator uses the string value as a namespace, false omits the namespace.

To configure you Action only you need to add the properties that MoleculerJS describes in the docs and the core 
automatically setup your settings:


```typescript
    @ServiceAction({
        params: {
            email: "string"
        },
        visibility: "published"
        
    })
    private findByEmailAction(ctx: Context): any {
        
    }
```

Important, by default the actions are registered with visibility "public", to expose an action to the 
middleware the action should visibility should be "published".


### Service Events

In the same way that you define actions you can define events, is basically the same, only you need to create a 
Service Events class in the Events directory and extends from [core/BaseServiceEvent](../src/core/BaseServiceEvent.ts), then go to the 
[events loader file](../src/events/loader/eventsLoader.ts), and register your ServiceEvent class. Check out the 
demonstration [AcmeEvents](../src/events/AcmeEvents.ts) for more details.   

The service event decorator [ServiceEvent](../src/core/decorators/ServiceEvent.ts) allows the same parameters, described 
in the [MoleculerJS Events](https://moleculer.services/docs/0.13/events.html) docs, plus name and namespace. The 
namespace parameter by default is true and by default use the events class name as a namespace, if  the namespace 
value is string, the decorator uses the string value as a namespace, false omits the namespace.

### Custom handlers 

You shouldn't create new service handlers but the option is open in case that you need to
do that. In case that you need to create a new service add a new file in the handlers directory 
called with this format `servicename.services.ts`, for example, `profile.service.ts`, then create a Class 
with the name of your service and extends from [core/BaseService](../src/core/BaseService.ts), and follow the same 
structure defined in the [core.service.ts](../src/handlers/core.service.ts) to create your custom service handler.