# Node Supervisor

This project is based on [MoleculerJS](https://moleculer.services/) is a progressive microservices framework for Node.js.
One of the features is that include built-in [Runner](https://moleculer.services/docs/0.13/runner.html). 

There is a project runner helper script in the Moleculer project. With it you don’t need to create a ServiceBroker 
instance with options, but a moleculer.config.js or moleculer.config.json file in the root of repo, fill it with your 
options, then call the moleculer-runner NPM script.

##Production-ready

```text
In production, we recommend putting options into the environment variables! Use the moleculer.config.js only 
during development or store only common options. Use the moleculer.config.js only if you need to subscribe to global 
service broker events.
```


```bash
$ moleculer-runner [options] [service files or directories or glob masks]
```

The runner does the following steps to load & merge configurations:

1- It loads config file defined in CLI options. If it does not exist, it throws an error.

2- If not defined, it loads the moleculer.config.js file from the current directory. If it does not exist, it loads 
the moleculer.config.json file.

3- Once a config file has been loaded, it merges options with the default options of the ServiceBroker.

4- The runner observes the options step by step and tries to overwrite them from environment variables. Once 
logLevel: "warn" is set in the config file, but the LOGLEVEL=debug environment variable is defined, the runner 
overwrites it, and it results: logLevel: "debug".


### Configuration file

The structure of the configuration file is the same as that of the [broker](https://moleculer.services/docs/0.13/broker.html#Broker-options) 
options. Every property has the same name.

**Example config file:**

```javascript
module.exports = {
    nodeID: "node-test",
    logger: true,
    logLevel: "debug",

    transporter: "amqp://rabbitmq:5672",
    requestTimeout: 5 * 1000,

    circuitBreaker: {
        enabled: true
    },

    metrics: true
};
```

### Environment variables

The runner transforms the property names to uppercase. If nested, the runner concatenates names with _.
    
**Example environment variables:**

```javascript
NODEID=node-test
LOGGER=true
LOGLEVEL=debug

# Shorthand transporter
TRANSPORTER=nats://localhost:4222
REQUESTTIMEOUT=5000

# Nested property
CIRCUITBREAKER_ENABLED=true

METRICS=true
```

### Handle restarts:

Since we are using Kubernetes aka Kb8 for the architecture orchestration, in case that an internal server error or 
critical exception occurs and the application jump to a crash state, the Kb8 container orchestration detects the issue,
terminating the current pod and restarting a new one. 

Use a ReplicationController, ReplicaSet, or Deployment for Pods that are not expected to terminate, for example, 
web servers. ReplicationControllers are appropriate only for Pods with a restartPolicy of Always.

A PodSpec has a restartPolicy field with possible values Always, OnFailure, and Never. The default value is Always. 
restartPolicy applies to all Containers in the Pod. restartPolicy only refers to restarts of the Containers by the 
kubelet on the same node. Exited Containers that are restarted by the kubelet are restarted with an exponential back-off
 delay (10s, 20s, 40s …) capped at five minutes, and is reset after ten minutes of successful execution. As discussed 
in the Pods document, once bound to a node, a Pod will never be rebound to another node.

For more details please reach out the Kubernetes [pod-lifecycle documentation](https://kubernetes.io/docs/concepts/workloads/pods/pod-lifecycle/).