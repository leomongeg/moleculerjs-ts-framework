# Base Microservice Project

## Project Stack

As you read in the MoleculerJs docs, you need to define a file or files to create you microservice, called in 
the following way: `myfirs.service.ts` or `myfirst.service.js`.

In that file or files, you have to define an object with the structure of your microservice. For obvious reasons, 
we decide to perform some changes in how we want to define the microservices and therefore in the project structure, 
in order to have the ability to use some nice components like Dependency Injection and other object-oriented techniques 
and patterns.

### Project Structure:

```
{project_root}
    |_src  // Our working directory
        |_actions
        |_core
        |_entity
        |_events
        |_handlers
            |_core.service.ts
        |_middlewares
        |_repositories
        |_services
        |_test
        |_types
        |_utils
    |_bash     
    |_docker
    |_docs
```  

- **src** contains all our working code, inside src we defined the project structure as:
    - **actions** contains all your microservices actions. Please see the [framework docs](framework.md) for details.
    - **core** contains the framework core classes (@TODO: working to move into npm package).
    - **events** contains all your microservices events. Please see the [framework docs](framework.md) for details
    - **entity** contains the ORM entity classes.
    - **handlers** contains all the MoleculerJS service classes there is one by default `core.service.ts`. Please see the [framework docs](framework.md) for details
    - **middlewares** contains all the MoleculerJS middlerwares files.
    - **repositories** contains the repository classes of our microservice project.
    - **services** contains the Dependency Injection services for our project.
    - **test** all the test cases, unit and integration test.
    - **utils** common utils libraries.
    
- **bash** bash configuration scripts.
- **docker** all the docker container config files and dependencies setup.
- **docs** all the project internal documentation (like this).

### TDD

The main idea is to strengthen the final product quality for that reason is required the unit testing for 
the development, so please make unit tests for every service that you develop in order to have the coverage 
of the 100% of the code in the tests. For more details please read the [Unit Testing Docs](unit_testing.md)

### Pull request strategy

Please create a feature branch for every JIRA ticket or issue, for example, `feature/CODE-1`. And when you finish 
the development create a Pull Request from your feature branch to the develop branch, please add the Tech Lead as a 
reviewer and other teammates in the project in order to help you to review your code.

When you going to create the PR, please attach a screenshot of the unit test coverage result like this:

![picture](assets/coverage.png)

In order your reviewers can take into account the coverage and request more tests or coverage. Or maybe helps you to 
remove unused code.

