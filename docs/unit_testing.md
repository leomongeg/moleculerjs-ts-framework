# Base Microservice Project

## Unit Testing

### Jest

This project uses Jest as the JavaScript and Typescript Testing Framework to ensure the correctness of the system.

[This](https://jestjs.io/) is the main Jest website which you can check for the official documentation.

### Get Started

In order to run the test please make sure that you are using the proper version of Node running:

```bash
$ nvm use
```

Then please install the project dependencies:

```bash
$ npm i
```

To run the project test suits run the following npm script:

```bash
$ npm run test
```

Now you are ready to start creating your test suits. First, be noticed that the projects has a directory called [test](../src/test)
inside the src directory, the main idea is to replicate exactly the same src structure and write your test according to 
your code files.

Please see the [AcmeActions.test](../src/test/actions/AcmeActions.test.ts) (src/test/actions/AcmeActions.test.ts) for 
more details.

### Suites Files Structure

1. Imports. Example:

```javascript
import dotenv from "dotenv";
import { ServiceBroker } from "moleculer";
```

2. Global variables. Example:

```javascript
dotenv.config({ path: ".env" });
const broker = new ServiceBroker();
```

3. Global Jest setup. Example:

```javascript
beforeAll(async () => {
    await broker.start();
    await InitMongoConnection.createMongoConnection(broker);
});
afterAll(() => broker.stop());
```

4. Test blocks. Each block should contain only related functionality which should be defined in single and atomic test cases. Example:

```javascript
describe("Block Description", () => {
    test("Adds 2 + 2 to equal 4", () => {
        expect(2 + 2).toBe(4);
    });
```

5. Test cases. Each test must only test one functionality. If the function under test have several return types or exceptions, you should force them and check each of them in separate test. Example:

```javascript
describe("Block Description", () => {
    test("Adds 2 + 2 to equal 4", () => {
        expect(2 + 2).toBe(4);
    });
    test("Adds 2 + 2 to not be 5", () => {
        expect(2 + 2).not.toBe(5);
    });
```
### Some Test Examples and Strategies.

Before starting to create your test, make sure to review the already implemented functions Jest have available. [Docs here](https://jestjs.io/docs/en/expect).
For example, you don't have to implement all of the checks since there are some matchers available.
```javascript
expect("Lorem ipsum").not.toMatch(/Z/i);
const usernames = ["john", "karen", "admin"];
expect(usernames).toContain("admin");
```

Test files should be located under the `/test` folder. Each module or service should have it's own test suite. Mock files should be organized in the same folder structure which they belong under `/src`, that folder name should be `__mocks__` since that's the Jest standard.

If you have an Async functionality, (very usual in API calls) you have to set the assertions count. Example:
```javascript
test("User fetched name should be Leanne Graham", async () => {
    expect.assertions(1);
    const data = await myServiceService.fetchAPIUser();
    expect(data.name).toEqual("Leanne Graham");
});
```

You can override any functionality with a Jest Mock Object. Make sure to check all the documentation on mock functions.
For example let's create an empty function stud which can be used to return an arbitrary value required for the test:
```javascript
test("Other mocks functionality", () => {
    const myMock = jest.fn();
    myMock
    .mockReturnValueOnce(10)
    .mockReturnValueOnce("x")
    .mockReturnValue(true);
    expect(myMock()).toBe(10);
    expect(myMock()).toBe("x");
    expect(myMock()).toBe(true);
    expect(myMock).toHaveBeenCalledTimes(3);
});
```

Mocking a module, service or class.

```javascript
// Imports the original module.
import ExampleService = require("../../handlers/myExample.service");
// Creates a mock generated module as a clone, with the same functions and class properties.
const mockExampleService: any = jest.genMockFromModule("../../handlers/myExample.service");

test("Mock from module", () => {
    // This method only returns the "lorem" string
    expect(exampleService.getLorem()).toBe("lorem");
    // Override the method with a mock function
    mockExampleService.getLorem = jest.fn(() => "mock ipsum");
    // The mock module works as the orinal with the overrides applied.
    expect(mockSillService.getLorem()).toBe("mock ipsum");
});
```

Timers. If a module implements a timer/halt/delay or any time restriction, you can force the completion so the test doesn't get affected by those wait times.
```javascript
test("Timers", () => {
    // Just a function with a very long execution time.
    fakeTimer(callback: any) {
        console.log("Ready....go!");
        setTimeout(() => {
            console.log("Times up -- stop!");
            callback && callback();
        }, 100000);
    }
    // The timers setup
    jest.useFakeTimers();
    fakeTimer(jest.fn());
    // Forces the timeout to finish immediately.
    jest.runAllTimers();
    expect(setTimeout).toHaveBeenCalledTimes(1);
});
```

To mock a database you need to override it's entity repository. And mock the service get repository function with the cloned one. Example:
```javascript
import { myExampleRepositoryMock } from "../__mocks__/repositories/MyExampleRepositoryMock";

test("Fake Database", async () => {
    exampleService.getMyExampleRepository = () => {
        return new MyExampleRepositoryMock();
    };
    const response = await broker.call("example.service.getAll");
    expect(response).toBeDefined();
});

//MyExampleRepositoryMock.ts
export class MyExampleRepositoryMock extends MyExampleRepository {
    getAll(id: string): any {
        return  [... mock entities here...];
```

Testing an Employee Portal Specific Microservice.

As suggestions on how to use specific Jest tests to maximize the code coverage here are some tips and examples that each suite should implement in order to guaranty that each service action works as expected. As a basic rule: each exposed action must have it's own set of tests and it should be tested as a broker call, and all it's possible outcomes should be checked for correctness. Examples:
```javascript
    // Let's make a call to the event action which gives all the users on the database.
    const response = await broker.call("users.service.getAll");

    // Now we should start checking for the expected values which make the test successful.
    expect(response).toBeDefined();

    // Before continue, have in mind that the service should return an object with the status and a data, and the data is an array with all the users found.

    // If the action returns a wrapper for the API call, we should check it's status.
    expect(response.status).toBeTruthy();

    // We can double check with other actions, to make sure all results are consistent between services.
    expect(response.data).toContain(firstUserOnDatabase);
    
    // You can also test and all of the items returned pass obligatory requirements.
    test.each(response.data)(
        'All items should be proper User entities',
        (user) => {
            expect(user).toBeInstanceOf(UserEntity);
        },
    );

    // You should check for essential or indispensable properties
    test.each(response.data)(
        'All items should have the email property',
        (user) => {
            expect(user.email).toBeDefined();
        },
    );
```

### Test results

After you finish to write your tests, you can execute the `$ npm run test` script to run the test suits created. The 
project has a preconfigured jest task to execute your test suits and generate a coverage report, please be noticed
the after run the test script the framework generates a directory called coverage with a test report in different
formats, included JSON, XML and HTML. In the same way the script generates a terminal output with the following format:     

```bash
$ npm run test

> base-microservices@0.1.0 test /Users/leonardom/Development/NodeJS Projects/base-microservice-framework
> jest --forceExit --coverage --verbose false

 PASS  src/test/actions/AcmeActions.test.ts


---------------------------|----------|----------|----------|----------|-------------------|
File                       |  % Stmts | % Branch |  % Funcs |  % Lines | Uncovered Line #s |
---------------------------|----------|----------|----------|----------|-------------------|
All files                  |    66.99 |    38.96 |     57.5 |    68.81 |                   |
 actions                   |      100 |    66.67 |      100 |      100 |                   |
  AcmeActions.ts           |      100 |    66.67 |      100 |      100 |                29 |
 actions/loader            |      100 |      100 |      100 |      100 |                   |
  actionsLoader.ts         |      100 |      100 |      100 |      100 |                   |
 config                    |      100 |      100 |      100 |      100 |                   |
  gateway.routes.ts        |      100 |      100 |      100 |      100 |                   |
 core                      |    57.48 |    26.67 |    51.61 |    59.84 |                   |
  BaseCoreService.ts       |       80 |      100 |    36.36 |    82.35 |... 87,113,122,135 |
  BaseService.ts           |       80 |      100 |       50 |       80 |                27 |
  BaseServiceActions.ts    |       50 |      100 |       25 |       50 |          25,34,47 |
  BaseServiceEvent.ts      |       75 |      100 |       50 |       75 |                25 |
  GatewayRoutingHandler.ts |    15.56 |       10 |       50 |    15.56 |... 13,114,129,130 |
  ServiceMetadataStore.ts  |     87.5 |       60 |      100 |      100 |       57,68,88,99 |
 core/decorators           |    77.19 |    45.45 |      100 |    78.18 |                   |
  ServiceAction.ts         |    82.61 |    44.44 |      100 |    86.36 |          38,47,51 |
  ServiceEvent.ts          |    73.53 |    46.15 |      100 |    72.73 |... 55,59,69,71,72 |
 events                    |    66.67 |      100 |        0 |    66.67 |                   |
  AcmeEvents.ts            |    66.67 |      100 |        0 |    66.67 |             21,22 |
 events/loader             |      100 |      100 |      100 |      100 |                   |
  eventsLoader.ts          |      100 |      100 |      100 |      100 |                   |
 handlers                  |      100 |      100 |    66.67 |      100 |                   |
  core.service.ts          |      100 |      100 |    66.67 |      100 |                   |
---------------------------|----------|----------|----------|----------|-------------------|

Test Suites: 1 passed, 1 total
Tests:       3 passed, 3 total
Snapshots:   0 total
Time:        2.558s
Ran all test suites.

```