# Publish routes to API Gateway

In order to expose your internal service actions, you can create an Routing object and the system automatically 
broadcast your routes to the available api gateways.

1- First make sure your service action has `visibility: "published"`.

2- Open the [gateway.routes.ts file](../src/config/gateway.routes.ts) defined in your config directory. In case that you 
are migration a version 1 of the framework to version 2 copy the template gateway route file defined in 
the [base-microservice-framework](https://bitbucket.criticalmass.com/projects/EP/repos/base-microservice-framework/browse) repository.

3- As you can see the [gateway.routes.ts file](../src/config/gateway.routes.ts) has a preconfigured properties:

```typescript
export const routing: GatewayRouting = {
    publisher            : "middleware.routing.publishRoutes",
    publisherAvailableEvt: "middleware.routing.publisherAvailable",
    routes               : [],
    unpublisher          : "middleware.routing.unpublishRoutes",
};
```

The `publisher` property defines the default gateway event handler to emit the routes to be published in the gateway.

The `unpubliser` property defines the default gateway event handler to emit the action to remove routes for the given 
current to the gateway.

The `publisherAvailableEvt`, when the api gateway is restarted or destroyed and restarted by Kb8, the api gateway needs 
tells the other microservices that is available to publish the routes again, this event allows you microservice to send 
the internal routes to be published in the api gateway.

The `routes` property array contain the list of routes to be published to the api gateway.

4- Create your routes, in the `routes` property array you can add a new route object to be published to the api gateway 
with the given structure:

```typescript
export const routing: GatewayRouting = {
    publisher  : "middleware.routing.publishRoutes",
    routes     : [
        {
            aliases  : {
                 "GET add" : "acmeAdd",

            },
            authorization: true,
            id           : "api",
        }
    ],
    unpublisher: "middleware.routing.unpublishRoutes",
};
```

The route `aliases` property define the HTTP verb and the path to be created in the Api gateway, then followed by the 
service action as is registered in the service broker.

You can define an special object to specify the roles required by the user to access your service action:

```typescript
aliases: {
    "GET add" : "acmeAdd",
    "POST update": [
        {className: "RolePermissionMiddleware", params: [{role: "HR"}]},
        "acmeUpdate"
    ],
``` 

as you can see in the above scope, the `POST update` route is an array, the first element is the middlware definition, 
an special object with the list of roles, for more details, please take a look at [Ecosystem Middleware](https://bitbucket.criticalmass.com/projects/EP/repos/cm-employee-middleware/browse) repository
and the las element is the service action, this is very important the last action always should be the service action.

The `authorization` property tells the api gateway that your routes requires authorization.

The `id` property is an unique identifier for your service routes in case that you have more of one routes objects.

The `bodyParsers`, allow you to define how the api gateway should parse the request body.

The `path` property allows your to override the default path. By default all your service routes starts with the service
name followed by the `path` property if path is omitted the default is `/api` for example `/acme/api`. For example, if you
define `path: "/config"` your route path is registered as '/acme/config' in the api gateway.

To use the api gateway middlewares you can use the property `use` this is an array property that allow a list of strings 
with the class representation name of the api gateway middleware. For more details, please checkout the [Ecosystem api gateway](https://bitbucket.criticalmass.com/projects/EP/repos/cm-employee-middleware/browse)
repository to know the available middlweares.

The `service` property allows you to define the service in charge to handle the service action by default is 
[core.service.ts](../src/handlers/core.service.ts)

In case that you need to define a different api gateway to broadcast your routes you can override the `publisher` and 
`unpublisher` properties inside of the route object.

This is a full example of the usage of the [gateway.routes.ts](../src/config/gateway.routes.ts) file

```typescript
export const routing: GatewayRouting = {
    publisher  : "middleware.routing.publishRoutes",
    routes     : [
        {
            aliases      : {
                "GET add"             : "acmeAdd",
                "GET myServiceAction ": [
                    {className: "RolePermissionMiddleware", params: [{role: "HR"}]},
                    "myServiceAction"
                ]
            },
            authorization: true,
            id           : "api",
            use          : [{className: "AuthMiddleware", params: [{role: "HR"}]}]
        },
        {
            aliases      : {
                "GET system-settings": "acmeSystemSettings"
            },
            authorization: false,
            bodyParsers  : {
                json      : true,
                urlencoded: {extended: true}
            },
            id           : "config",
            path         : "/config",
            publisher  : "settingsgateway.routing.publishRoutes",
            unpublisher: "settingsgateway.routing.unpublishRoutes",
        }
    ],
    unpublisher: "middleware.routing.unpublishRoutes",
};
```

For more details, please check out the [MoleculerJS API Gateway docs](https://moleculer.services/docs/0.12/moleculer-web.html)

    
 